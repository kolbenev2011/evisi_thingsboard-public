
/*
* Use this function to convert a cell into a horizontal bar. Take into account that value must be a number between 0 and 100
* Use the desired color by changing the color value by the one of your preference
*	When using in Thingsboard, copy and paste only the function's content
*/
function cellStyle(value) {
	var color = "limegreen";
	var gradient = "linear-gradient(to right, " + color + " "+ value +"%, transparent 0%)";
	return {
		"background": gradient,
		"color": "#333",
		"text-align": "center",
  	"background-clip": "content-box"
	};
}



/*
* Use this function to format a cell's content as a percentage
* Set the decimals value with the number of decimals you wish to display
*	When using in Thingsboard, copy and paste only the function's content
*/
function cellContent(value, entity, filter) {
	var decimals = 2;
	return value.toFixed(decimals) + '%';
}
