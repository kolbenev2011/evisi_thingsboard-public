function transform(msg, metadata, msgType) {

	// Get battery output energy array from metadata
	var bat_1_output_energy_data = JSON.parse(metadata.bat_1_outputEnergy_i);

	// Calculate battery output total for the given period
	var bat_1_output_energy_Wh_total = Array.isArray(bat_1_output_energy_data)
		  ? bat_1_output_energy_data.reduce(function(a, b) {
		          return a + b.value
		      }, 0)
		  : parseFloat(bat_1_output_energy_data);

	// Get vehicule distance array from metadata
	var veh_1_distance_data = JSON.parse(metadata.veh_1_distance_i);

	// Calculate battery output total for the given period
	var veh_1_distance_total = Array.isArray(veh_1_distance_data)
		  ? veh_1_distance_data.reduce(function(a, b) {
		          return a + b.value
		      }, 0)
		  : parseFloat(veh_1_distance_data);

	// Convert Wh to kWh and m to km
	// Calculate avg energy output per Km 
	var veh_1_kWhkm_i = (bat_1_output_energy_Wh_total / 1000) / (veh_1_distance_total / 1000);

	// Setup output message
	var outputMsg = {
		  "veh_1_kWhkm_i": veh_1_kWhkm_i
	}

	// Remove battery output energy and distance arrays from metadata
	metadata.bat_1_outputEnergy_i = null;
	metadata.veh_1_distance_i = null;
	 
	// Return message
	return {msg: outputMsg, metadata: metadata, msgType: msgType};
}
