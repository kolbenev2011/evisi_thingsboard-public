function transform(msg, metadata, msgType) {
	// Get battery state of charge from msg
	var bat_1_soc = msg.bat_1_soc;

	// Get latest calculation for battery consumption per km from metadata
	var veh_1_kWhkm_i = JSON.parse(metadata.veh_1_kWhkm_i);

	// Get battery capacity from metadata
	var bat_1_batCapacity = JSON.parse(metadata.bat_1_batCapacity);

	// Get remaining charge
	var remainingCharge = bat_1_soc * bat_1_batCapacity / 100;

	// Calculate remaining Distance
	var remaningDistance = remainingCharge / veh_1_kWhkm_i;

	msg.veh_1_distanceRemaing_i = remaningDistance;
		  
	// Create output message
	return {msg: msg, metadata: metadata, msgType: msgType};
}
