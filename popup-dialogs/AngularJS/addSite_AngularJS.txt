<md-dialog aria-label="Add entity" style="width: 480px">
    <form name="addAssetForm" ng-submit="vm.save()" onload="vm.formLoaded()">
        <md-toolbar>
            <div class="md-toolbar-tools">
                <h2>Add asset</h2>
                <span flex></span>
                <md-button class="md-icon-button" ng-click="vm.cancel()">
                    <ng-md-icon icon="close" aria-label="Close"></ng-md-icon>
                </md-button>
            </div>
        </md-toolbar>
        <md-progress-linear class="md-warn" md-mode="indeterminate" ng-disabled="!$root.loading && !vm.loading" ng-show="$root.loading || vm.loading"></md-progress-linear>
        <span style="min-height: 5px;" flex="" ng-show="!$root.loading && !vm.loading"></span>
        <md-dialog-content>
            <div class="md-dialog-content">
                <fieldset ng-disabled="$root.loading || vm.loading">
                    <div flex layout="row">
                    <md-input-container flex="33" class="md-block">
                        <label>Asset name</label>
                        <input ng-model="vm.assetName" name="assetName" required ng-disabled="true">
                        <div ng-messages="addAssetForm.assetName.$error">
                            <div ng-message="required">Asset name is required.</div>
                        </div>
                    </md-input-container>
                    <md-input-container flex="66" class="md-block">
                            <label>Asset Label</label>
                            <input name="assetLabel" ng-model="vm.assetLabel">
                        </md-input-container>
<!--                     <tb-entity-subtype-autocomplete
                        id="assetType"
                        flex="33"
                        tb-required="true"
						ng-disabled="true"
                        the-form="addAssetForm"
                        ng-model="vm.assetType"
                        entity-type="vm.types.entityType.asset">
                    </tb-entity-subtype-autocomplete> -->
                    </div>
<!--                         <tb-entity-group-autocomplete flex
                        ng-model="vm.assetGroupId"
                        tb-required="true"
						ng-disabled="true"
                        group-type="vm.types.entityType.asset"
                        the-form="addAssetForm"
                        placeholder-text="Asset group"
                        not-found-text="entity-group.no-entity-groups-matching"
                        required-text="entity-group.target-entity-group-required">
                    </tb-entity-group-autocomplete> -->
                    <div flex layout="row">
                        <md-input-container flex="50" class="md-block">
                            <label>Latitude</label>
                            <input type="number" step="any" name="latitude" ng-model="vm.attributes.latitude">
                        </md-input-container>
                        <md-input-container flex="50" class="md-block">
                            <label>Longitude</label>
                            <input type="number" step="any" name="longitude" ng-model="vm.attributes.longitude">
                        </md-input-container>
                    </div>
					<div flex layout="row">
                        <md-input-container flex="100" class="md-block">
                            <label>Contact Name</label>
                            <input name="contactName" ng-model="vm.attributes.contactName">
                        </md-input-container>
                    </div>
					<div flex layout="row">
						<md-input-container flex="50" class="md-block">
                            <label>Contact Email</label>
                            <input name="contactEmail" ng-model="vm.attributes.contactEmail">
                        </md-input-container>
						<md-input-container flex="50" class="md-block">
                            <label>Contact Phone</label>
                            <input name="contactPhone" ng-model="vm.attributes.contactPhone">
                        </md-input-container>
					</div>
					<div flex layout="row">
						<md-input-container flex="100" class="md-block">
						<label>Address</label>
						<input name="address" ng-model="vm.attributes.address">
                        </md-input-container>
					</div>
					<div flex layout="row">
						<md-input-container flex="100" class="md-block">
						<label>Site Map Url</label>
						<input name="siteMapUrl" ng-model="vm.attributes.siteMapUrl">
                        </md-input-container>
					</div>
					<div flex layout="column">
						<md-button flex="0 0 100%" ng-click="vm.addPolygonCoord()" class="md-primary">Add Coord</md-button>
						<md-content id="scrollableCoords" flex="none">
							<div flex layout="row" ng-repeat="coordPair in vm.attributes.geofence">
							    <label flex="50" class="coordLabel md-block">Coord {{$index + 1}}: </label>
								<md-input-container flex="50" class="md-block">
									<label>Latitude</label>
									<input type="number" step="any" name="latitude" ng-model='vm.attributes.geofence[$index][0]'>
								</md-input-container>
								<md-input-container flex="50" class="md-block">
									<label>Longitude</label>
									<input type="number" step="any" name="longitude" ng-model="vm.attributes.geofence[$index][1]">
								</md-input-container>
						<md-button flex="none"  ng-click="vm.removePolygonCoord($index)" class="removeCoordBtn md-primary">X</md-button>
							</div>
						</md-content>
					</div>
                </fieldset>
            </div>
        </md-dialog-content>
        <md-dialog-actions>
            <md-button type="submit" ng-disabled="vm.loading || addAssetForm.$invalid || !addAssetForm.$dirty" class="md-raised md-primary">Create</md-button>
            <md-button ng-click="vm.cancel()" class="md-primary">Cancel</md-button>
        </md-dialog-actions>
    </form>
</md-dialog>







let $injector = widgetContext.$scope.$injector;
let $mdDialog = $injector.get('$mdDialog'),
    $document = $injector.get('$document'),
    $q = $injector.get('$q'),
    $rootScope = $injector.get('$rootScope'),
    types = $injector.get('types'),
    assetService = $injector.get('assetService'),
    attributeService = $injector.get('attributeService'),
    entityGroupService = $injector.get('entityGroupService'),
    customerService = $injector.get('customerService');

openAddAssetDialog();

function openAddAssetDialog() {
    $mdDialog.show({
        controller: ['$scope','$mdDialog', AddAssetDialogController],
        controllerAs: 'vm',
        template: htmlTemplate,
        parent: angular.element($document[0].body),
        targetEvent: $event,
        multiple: true,
        clickOutsideToClose: false
    });
}

function AddAssetDialogController($scope, $mdDialog) {
    let vm = this;
    vm.types = types;
    vm.assetType = "Site";
    vm.attributes = {geofence:[]};
    vm.assetGroupId = "25131150-3658-11ea-a289-9f334be5daf0";
	
    vm.cancel = () => {
        $mdDialog.hide();
    };
    
    vm.addPolygonCoord = () => {
        vm.attributes.geofence.push([0,0]);
    };
    
    vm.removePolygonCoord = (index) => {
        vm.attributes.geofence.splice(index, 1);
    };
    
    vm.save = () => {
        vm.loading = true;
		vm.attributes.geofence = JSON.stringify(vm.attributes.geofence);
        $scope.addAssetForm.$setPristine();
        let asset = {
            name: vm.assetName,
            type: vm.assetType,
            label: vm.assetLabel
        };
        assetService.saveAsset(asset, null, null, vm.assetGroupId).then(
            (asset) => {
                saveAttributes(asset.id).then(
                    () => {
                        vm.loading = false;
                        updateAliasData();
                        $mdDialog.hide();
                    }
                );
            },
            () => {
                vm.loading = false;
            }
        );
    };
    
    function saveAttributes(entityId) {
        let attributesArray = [];
        for (let key in vm.attributes) {
            attributesArray.push({key: key, value: vm.attributes[key]});
        }
        if (attributesArray.length > 0) {
            return attributeService.saveEntityAttributes(entityId.entityType, entityId.id, "SERVER_SCOPE", attributesArray);
        } else {
            return $q.when([]);
        }
    }
    
    function updateAliasData() {
        let aliasIds = [];
        for (let id in widgetContext.aliasController.resolvedAliases) {
            aliasIds.push(id);
        }
        let tasks = [];
        aliasIds.forEach((aliasId) => {
            widgetContext.aliasController.setAliasUnresolved(aliasId);
            tasks.push(widgetContext.aliasController.getAliasInfo(aliasId));
        });
        $q.all(tasks).then(() => {
            $rootScope.$broadcast('widgetForceReInit');
        });
    }
}

#assetType button {
    display: none;
}

md-autocomplete {
    min-width: 0px;
}

#scrollableCoords {
    height: 300px;
    overflow: auto;
    background-color: white;
}

.removeCoordBtn {
    height: 50%;
    flex: 0 10 100%;
    margin-top: 6px;
    padding-bottom: 6px;
    padding-top: 3px;
}

#scrollableCoords .md-errors-spacer {
    min-height: 0px;
}

.coordLabel {
    text-align: center;
    width: fit-content;
    height: fit-content;
    flex: 0 0 0;
    margin-right: 20px;
    align-self: center;
}

input[readonly] {
    background-color: #f0f0f0;
    border-radius: 3px;
}




