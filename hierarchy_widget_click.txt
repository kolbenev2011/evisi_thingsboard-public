debugger;
let $injector = widgetContext.$scope.$injector;
let assetService = $injector.get(widgetContext.$scope.ctx.servicesMap.get('assetService'));
let deviceService = $injector.get(widgetContext.$scope.ctx.servicesMap.get('deviceService'));
let http = $injector.get(widgetContext.$scope.ctx.servicesMap.get('http'));

deviceService.getDevice(entityId.id).subscribe(function(entity) {
	let id1 = null, 
	id2 = null, 
	targetDashboardId = "",
	paramName = null,
	targetEntityId = null,
	targetEntityName = null,
	targetEntityLabel = null;

	if (entity.type == "evcharger") {
		id1 = "chargersData";
		id2 = "selectedCharger";
		targetDashboardId = "e70aa640-e8de-11ea-a2e1-3da06e962d6c";
		
		//Set clicked entity as target state entity
		targetEntityId = entityId;
		targetEntityLabel = entityLabel;
		targetEntityName = entityName;
    } else if (entity.type == "BUS") {
		id1 = "vehicle_list";
		id2 = "vehicle_motor";
		targetDashboardId = "f313b440-e8de-11ea-a2e1-3da06e962d6c";
		
		//Set clicked entity as target state entity
		targetEntityId = entityId;
		targetEntityLabel = entityLabel;
		targetEntityName = entityName;
    } else if (entity.type == "hierarchy_node") {
		let siteAliasId = widgetContext.aliasController.getEntityAliasId("specificSite");
		let siteEntity = widgetContext.dashboard.aliasController.getInstantAliasInfo(siteAliasId);
		targetEntityId = {
				id:siteEntity.currentEntity.id,
				entityType:siteEntity.currentEntity.entityType,
			};
		targetEntityName = siteEntity.currentEntity.name;
		targetEntityLabel = siteEntity.currentEntity.label;
		paramName = 'selectedSite';
					
		if (entityName == "node_evchargers") {
			id1 = "siteChargersList";
			targetDashboardId ="e70aa640-e8de-11ea-a2e1-3da06e962d6c";
		} else if(entityName == "node_vehicles") {
			id1 = "site_vehicles_list";
			targetDashboardId ="f313b440-e8de-11ea-a2e1-3da06e962d6c";
		}
	}

	var states = [];
	var paramsTemplate = {
		entityId: targetEntityId,
		entityName: targetEntityName,
		entityLabel: targetEntityLabel
	};
	var params = {};
	if (paramName) {
		//Dashboard state entity WITH params - Set entity data at the param name level (can specify multiple state entities)
		params["targetEntityParamName"] = paramName;
		params[paramName] = paramsTemplate;
	} else {
		//Dashboard state entity WITHOUT params - Entity data at the root
		params = paramsTemplate;
	}
	
	states.push( {
			id: id1,
			params: (id2) ? {} : params //Include above entity params if id1 is the target state.
	});
    if (id2) {
		states.push( {
			id: id2,
			params: params //Include above entity params if id2 is the target state.
		});
    }
	
	var state = btoa(JSON.stringify(states));
        
	http.get("/api/permissions/allowedPermissions").subscribe((res) => {
		let isSinglePage = null;
			if (res.userOwnerId.entityType == "TENANT"){
				isSinglePage = false;
			} else {
				isSinglePage = true;
			}
			
			if (isSinglePage) {
			  url = `/dashboard/${targetDashboardId}?state=${state}`;
			} else {
			  url = `/dashboards/${targetDashboardId}?state=${state}`;
			}
			widgetContext.dashboardService.router.navigateByUrl(url);
		 });
});